# README #

In questo repository si trovano la cartella Dataset Spotify in cui sono contenuti tutti i file con i record contenenti il dataset
(per dettagli sul dataset leggere il README.md nella cartella citata) e il file .ipynb contenente tutto il codice e la relazione
del progetto.

L'obiettivo del progetto era quello di predire se una canzone di Spotify sarebbe stata una hit oppure no e per farlo sono stati creati
diversi modelli di classificazione binaria utilizzando come algoritmi Perceptron, Logistic Regression, SVM,  Decision Tree, Random
Forest, XGBoost e poi messi a confronto.
